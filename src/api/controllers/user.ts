import express from "express";
import UserModel from "../models/user";
import { IUser } from "../interfaces/user";

export default class UserController {
    static async getAll(req: express.Request, res: express.Response) {
        try {
            const users = await UserModel.getAll() as IUser[]
            const response = {
                success: true,
                data: users
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async getById(req: express.Request, res: express.Response) {
        try {
            const userId = req.params.id;
            if (userId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const user = await UserModel.getById(userId) as IUser;
            const response = {
                success: true,
                data: user
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async create(req: express.Request, res: express.Response) {
        try {
            const payload: IUser = await req.body;
            const response = await UserModel.create({
                ...payload
            })
            return res.status(201).send({
                success: true,
                data: response,
                message: "User created successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async patch(req: express.Request, res: express.Response) {
        try {
            const userId = req.params.id;
            if (userId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const payload: IUser = await req.body;
            const response = await UserModel.patch(userId, payload)
            return res.status(201).send({
                success: true,
                data: response,
                message: "User updated successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async delete(req: express.Request, res: express.Response) {
        try {
            const userId = req.params.userId;
            if (userId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const response = await UserModel.delete(userId);
            return response.status(200).send({
                success: true,
                message: "User deleted succesfully"
            });
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }
}