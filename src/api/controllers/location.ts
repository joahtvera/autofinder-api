import express from 'express';
import LocationModel from '../models/location'
import { ILocation } from '../interfaces/location';

export default class LocationController {

    static async getAll(req: express.Request, res: express.Response) {
        try {
            const locations = await LocationModel.getAll() as ILocation[]
            const response = {
                success: true,
                data: locations
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async getById(req: express.Request, res: express.Response) {
        try {
            const locationId = req.params.id;
            if (locationId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const location = await LocationModel.getById(locationId) as ILocation;
            const response = {
                success: true,
                data: location
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async create(req: express.Request, res: express.Response) {
        try {
            const payload: ILocation = await req.body;
            const response = await LocationModel.create({
                ...payload
            })
            return res.status(201).send({
                success: true,
                data: response,
                message: "Location created successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async patch(req: express.Request, res: express.Response) {
        try {
            const locationId = req.params.id;
            if (locationId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const payload: ILocation = await req.body;
            const response = await LocationModel.patch(locationId, payload)
            return res.status(201).send({
                success: true,
                data: response,
                message: "Location updated successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async delete(req: express.Request, res: express.Response) {
        try {
            const locationId = req.params.locationId;
            if (locationId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const response = await LocationModel.delete(locationId);
            return response.status(200).send({
                success: true,
                message: "Location deleted succesfully"
            });
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }
}