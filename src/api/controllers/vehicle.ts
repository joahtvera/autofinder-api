import express from 'express'
import { IVehicle } from '../interfaces/vehicle'
import VehicleModel from '../models/vehicle'

export default class VehicleController {

    static async getAll(req: express.Request, res: express.Response) {
        try {
            const vehicles = await VehicleModel.getAll() as IVehicle[]
            const response = {
                success: true,
                data: vehicles
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async getById(req: express.Request, res: express.Response) {
        try {
            const vehicleId = req.params.id;
            if (vehicleId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const todo = await VehicleModel.getById(vehicleId) as IVehicle;
            const response = {
                success: true,
                data: todo
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async create(req: express.Request, res: express.Response) {
        try {
            const payload: IVehicle = await req.body;
            const response = await VehicleModel.create({
                ...payload
            })
            return res.status(201).send({
                success: true,
                data: response,
                message: "Vehicle created successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async patch(req: express.Request, res: express.Response) {
        try {
            const payload = req.body;
            const vehicleId = req.params.vehicleId;
            if (vehicleId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const response = await VehicleModel.patch(vehicleId, payload);
            return response.status(200).send({
                success: true,
                data: response,
                message: "Vehicle updated succesfully"
            });
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async delete(req: express.Request, res: express.Response) {
        try {
            const vehicleId = req.params.vehicleId;
            if (vehicleId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const response = await VehicleModel.delete(vehicleId);
            return response.status(200).send({
                success: true,
                message: "Vehicle deleted succesfully"
            });
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }
}