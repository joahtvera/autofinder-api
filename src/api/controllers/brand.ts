import express from 'express';
import BrandModel from '../models/brand'
import { IBrand } from '../interfaces/brand';

export default class BrandController {

    static async getAll(req: express.Request, res: express.Response) {
        try {
            const brands = await BrandModel.getAll() as IBrand[]
            const response = {
                success: true,
                data: brands
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async getById(req: express.Request, res: express.Response) {
        try {
            const brandId = req.params.id;
            if (brandId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const brand = await BrandModel.getById(brandId) as IBrand;
            const response = {
                success: true,
                data: brand
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async create(req: express.Request, res: express.Response) {
        try {
            const payload: IBrand = await req.body;
            const response = await BrandModel.create({
                ...payload
            })
            return res.status(201).send({
                success: true,
                data: response,
                message: "Brand created successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async patch(req: express.Request, res: express.Response) {
        try {
            const brandId = req.params.id;
            if (brandId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const payload: IBrand = await req.body;
            const response = await BrandModel.patch(brandId, payload)
            return res.status(201).send({
                success: true,
                data: response,
                message: "Brand updated successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async delete(req: express.Request, res: express.Response) {
        try {
            const brandId = req.params.brandId;
            if (brandId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const response = await BrandModel.delete(brandId);
            return response.status(200).send({
                success: true,
                message: "Brand deleted succesfully"
            });
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }
}