import express from 'express'
import { uploadImage } from '../../utils/cloudinary'

export default class UploadController {

    static async uploadImage(req: express.Request, res: express.Response) {
        try {
            const data = await req.body
            return
            const image = data.get('image') as File
            if (!data) return
            const preBuffer = await image.arrayBuffer()
            const buffer = Buffer.from(preBuffer)
            const response = await uploadImage(buffer)
            console.log(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }
}