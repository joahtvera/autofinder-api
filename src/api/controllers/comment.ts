import express from "express";
import { IComment } from "../interfaces/comment";
import CommentModel from "../models/comment";

export default class CommentController {
    static async getAll(req: express.Request, res: express.Response) {
        try {
            const comments = await CommentModel.getAll() as IComment[]
            const response = {
                success: true,
                data: comments
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async getById(req: express.Request, res: express.Response) {
        try {
            const commentId = req.params.id;
            if (commentId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const comment = await CommentModel.getById(commentId) as IComment;
            const response = {
                success: true,
                data: comment
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async create(req: express.Request, res: express.Response) {
        try {
            const payload: IComment = await req.body;
            const response = await CommentModel.create({
                ...payload
            })
            return res.status(201).send({
                success: true,
                data: response,
                message: "Comment created successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async patch(req: express.Request, res: express.Response) {
        try {
            const commentId = req.params.id;
            if (commentId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const payload: IComment = await req.body;
            const response = await CommentModel.patch(commentId, payload)
            return res.status(201).send({
                success: true,
                data: response,
                message: "Comment updated successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async delete(req: express.Request, res: express.Response) {
        try {
            const commentId = req.params.commentId;
            if (commentId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const response = await CommentModel.delete(commentId);
            return response.status(200).send({
                success: true,
                message: "Comment deleted succesfully"
            });
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }
}