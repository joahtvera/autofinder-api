import express from 'express';
import CategoryModel from '../models/category'
import { ICategory } from '../interfaces/category';

export default class CategoryController {

    static async getAll(req: express.Request, res: express.Response) {
        try {
            const categories = await CategoryModel.getAll() as ICategory[]
            const response = {
                success: true,
                data: categories
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async getById(req: express.Request, res: express.Response) {
        try {
            const categoryId = req.params.id;
            if (categoryId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const brand = await CategoryModel.getById(categoryId) as ICategory;
            const response = {
                success: true,
                data: brand
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async create(req: express.Request, res: express.Response) {
        try {
            const payload: ICategory = await req.body;
            const response = await CategoryModel.create({
                ...payload
            })
            return res.status(201).send({
                success: true,
                data: response,
                message: "Category created successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async patch(req: express.Request, res: express.Response) {
        try {
            const categoryId = req.params.id;
            if (categoryId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const payload: ICategory = await req.body;
            const response = await CategoryModel.patch(categoryId, payload)
            return res.status(201).send({
                success: true,
                data: response,
                message: "Category updated successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async delete(req: express.Request, res: express.Response) {
        try {
            const categoryId = req.params.categoryId;
            if (categoryId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const response = await CategoryModel.delete(categoryId);
            return response.status(200).send({
                success: true,
                message: "Category deleted succesfully"
            });
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }
}