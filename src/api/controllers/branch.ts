import express from "express";
import BranchModel from "../models/branch";
import { IBranch } from "../interfaces/branch";

export default class BranchController {
    static async getAll(req: express.Request, res: express.Response) {
        try {
            const branches = await BranchModel.getAll() as IBranch[]
            const response = {
                success: true,
                data: branches
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(404).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async getById(req: express.Request, res: express.Response) {
        try {
            const branchId = req.params.id;
            if (branchId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const branch = await BranchModel.getById(branchId) as IBranch;
            const response = {
                success: true,
                data: branch
            }
            return res.status(200).send(response)
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async create(req: express.Request, res: express.Response) {
        try {
            const payload: IBranch = await req.body;
            const response = await BranchModel.create({
                ...payload
            })
            return res.status(201).send({
                success: true,
                data: response,
                message: "Branch created successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async patch(req: express.Request, res: express.Response) {
        try {
            const branchId = req.params.id;
            if (branchId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const payload: IBranch = await req.body;
            const response = await BranchModel.patch(branchId, payload)
            return res.status(201).send({
                success: true,
                data: response,
                message: "Branch updated successfully"
            })
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }

    static async delete(req: express.Request, res: express.Response) {
        try {
            const branchId = req.params.branchId;
            if (branchId === undefined)
                return res.status(418).send({
                    success: false,
                    message: "No valid identifier provided"
                });
            const response = await BranchModel.delete(branchId);
            return response.status(200).send({
                success: true,
                message: "Branch deleted succesfully"
            });
        } catch (error) {
            console.log((error as Error).message)
            return res.status(400).send({
                success: false,
                message: (error as Error).message.toUpperCase()
            })
        }
    }
}