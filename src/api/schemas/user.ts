import { Model, Schema, model, models } from "mongoose";
import { IUser } from "../interfaces/user";

interface IUserModel extends Model<IUser> { }

const userModel = new Schema<IUser, IUserModel>({
    username: { type: String, required: true, maxlength: 30 },
    email: {
        type: String,
        unique: true,
        required: true
    },
    favoriteList: Array,
    password: { type: String, required: true },
}, {
    timestamps: true
});

export default models.User || model<IUser, IUserModel>("users", userModel)