import { Model, Schema, model, models } from "mongoose";
import { IComment } from "../interfaces/comment";

interface ICommentModel extends Model<IComment> { }

const commentModel = new Schema<IComment, ICommentModel>({
    title: { type: String, required: true, maxlength: 30 },
    body: { type: String, required: true, maxlength: 300 },
    archived: { required: true, type: Boolean },
    createdBy: { type: String, required: true }
}, {
    timestamps: true
});

export default models.Comment || model<IComment, ICommentModel>("comments", commentModel)