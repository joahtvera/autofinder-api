import { Model, Schema, model, models } from "mongoose";
import { IBranch } from "../interfaces/branch";
interface IBranchModel extends Model<IBranch> { }

const branchModel = new Schema<IBranch, IBranchModel>({
    name: { type: String, required: true, maxlength: 30 },
    addres: { type: String, required: true, maxlength: 50 },
    location: { type: Array, required: true }
}, {
    timestamps: true
});

export default models.Branch || model<IBranch, IBranchModel>("branches", branchModel)