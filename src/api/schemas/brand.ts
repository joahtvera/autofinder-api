import { Model, Schema, model, models } from "mongoose";
import { IBrand } from "../interfaces/brand";

interface IBrandModel extends Model<IBrand> { }

const brandModel = new Schema<IBrand, IBrandModel>({
    name: { type: String, required: true, maxlength: 30 },
    image: String
}, {
    timestamps: true
});

export default models.Brand || model<IBrand, IBrandModel>("brands", brandModel)