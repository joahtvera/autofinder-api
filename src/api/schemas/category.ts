import { Model, Schema, model, models } from "mongoose";
import { ICategory } from "../interfaces/category";

interface ICategoryModel extends Model<ICategory> { }

const categoryModel = new Schema<ICategory, ICategoryModel>({
    name: { type: String, required: true, maxlength: 30 },
    categoryImage: { type: String, required: true}
}, {
    timestamps: true
});

export default models.Category || model<ICategory, ICategoryModel>("categories", categoryModel)