import { Model, Schema, model, models } from "mongoose";
import { ILocation } from "../interfaces/location";

interface ILocationModel extends Model<ILocation> { }

const locationModel = new Schema<ILocation, ILocationModel>({
    name: { type: String, required: true },
    latitude: { type: Number, required: true },
    longitude: { type: Number, required: true }
}, {
    timestamps: true
});

export default models.Location || model<ILocation, ILocationModel>("locations", locationModel);