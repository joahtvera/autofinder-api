import { Model, Schema, model, models } from "mongoose";
import { IVehicle } from "../interfaces/vehicle";

interface IVehicleModel extends Model<IVehicle> { }

const vehicleSchema = new Schema<IVehicle, IVehicleModel>({
    brand: { type: String, required: true, maxlength: 30 },
    category: { type: String, required: true, maxlength: 30 },
    year: { type: Number, required: true, maxlength: 4 },
    model: { type: String, required: true, maxlength: 30 },
    imageUrl: { type: String, required: true },
    branch: Array,
    archived: { required: true, type: Boolean },
}, {
    timestamps: true
});

export default models.Vehicle || model<IVehicle, IVehicleModel>("vehicles", vehicleSchema)