import { connectToDb } from "../../utils/db";
import { IUser } from "../interfaces/user";
import User from "../schemas/user"

export default class UserModel {

    static async getAll() {
        await connectToDb();
        return await User.find();
    }

    static async getById(userId: string) {
        await connectToDb();
        return await User.findById(userId);
    }

    static async create(user: IUser) {
        await connectToDb();
        const newUser = new User(user);
        return await newUser.save();
    }

    static async patch(userId: string, payload: IUser) {
        await connectToDb();
        return await User.findByIdAndUpdate(userId, payload);
    }

    static async delete(userId: string) {
        await connectToDb();
        return await User.findByIdAndDelete({ _id: userId });
    }

    static async deleteMany(userIds: string[]) {
        await connectToDb();
        return await User.deleteMany({ _id: { $in: userIds } })
    }
}