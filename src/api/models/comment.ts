import { connectToDb } from "../../utils/db";
import { IComment } from '../interfaces/comment'
import Comment from '../schemas/comment'

export default class CommentModel {

    static async getAll() {
        await connectToDb();
        return await Comment.find();
    }

    static async getById(commentId: string) {
        await connectToDb();
        return await Comment.findById(commentId);
    }

    static async create(comment: IComment) {
        await connectToDb();
        const newComment = new Comment(comment);
        return await newComment.save();
    }

    static async patch(commentId: string, payload: IComment) {
        await connectToDb();
        return await Comment.findByIdAndUpdate(commentId, payload);
    }

    static async delete(commentId: string) {
        await connectToDb();
        return await Comment.findByIdAndDelete({ _id: commentId });
    }

    static async deleteMany(commentIds: string[]) {
        await connectToDb();
        return await Comment.deleteMany({ _id: { $in: commentIds } })
    }
}