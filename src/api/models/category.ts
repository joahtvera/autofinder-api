import { connectToDb } from "../../utils/db";
import { ICategory } from "../interfaces/category";
import Category from "../schemas/category";

export default class CategoryModel {

    static async getAll() {
        await connectToDb();
        return await Category.find();
    }

    static async getById(categoryId: string) {
        await connectToDb();
        return await Category.findById(categoryId);
    }

    static async create(category: ICategory) {
        await connectToDb();
        const newCategory = new Category(category);
        return await newCategory.save();
    }

    static async patch(categoryId: string, payload: ICategory) {
        await connectToDb();
        return await Category.findByIdAndUpdate(categoryId, payload);
    }

    static async delete(categoryId: string) {
        await connectToDb();
        return await Category.findByIdAndDelete({ _id: categoryId });
    }
}