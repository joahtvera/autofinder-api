import { connectToDb } from "../../utils/db";
import { IBranch } from "../interfaces/branch";
import Branch from "../schemas/branch";

export default class StudentModel {

    static async getAll() {
        await connectToDb();
        return await Branch.find();
    }

    static async getById(branchId: string) {
        await connectToDb();
        return await Branch.findById(branchId);
    }

    static async create(branch: IBranch) {
        await connectToDb();
        const newBranch = new Branch(branch);
        return await newBranch.save();
    }

    static async patch(branchId: string, payload: IBranch) {
        await connectToDb();
        return await Branch.findByIdAndUpdate(branchId, payload);
    }

    static async delete(branchId: string) {
        await connectToDb();
        return await Branch.findByIdAndDelete({ _id: branchId });
    }

    static async deleteMany(branchIds: string[]) {
        await connectToDb();
        return await Branch.deleteMany({ _id: { $in: branchIds } })
    }
}