import { connectToDb } from "../../utils/db";
import { IBrand } from "../interfaces/brand";
import Brand from "../schemas/brand";

export default class BrandModel {

    static async getAll() {
        await connectToDb();
        return await Brand.find();
    }

    static async getById(brandId: string) {
        await connectToDb();
        return await Brand.findById(brandId);
    }

    static async create(brand: IBrand) {
        await connectToDb();
        const newBrand = new Brand(brand);
        return await newBrand.save();
    }

    static async patch(brandId: string, payload: IBrand) {
        await connectToDb();
        return await Brand.findByIdAndUpdate(brandId, payload);
    }

    static async delete(brandId: string) {
        await connectToDb();
        return await Brand.findByIdAndDelete({ _id: brandId });
    }
}