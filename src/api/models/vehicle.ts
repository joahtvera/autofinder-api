import { connectToDb } from "../../utils/db";
import { IVehicle } from "../interfaces/vehicle";
import Vehicle from '../schemas/vehicle'

export default class VehicleModel {

    static async getAll() {
        await connectToDb();
        return await Vehicle.find();
    }

    static async getById(vehicleId: string) {
        await connectToDb();
        return await Vehicle.findById(vehicleId);
    }

    static async create(vehicle: IVehicle) {
        await connectToDb();
        const newVehicle = new Vehicle(vehicle);
        return await newVehicle.save();
    }

    static async patch(vehicleId: string, payload: IVehicle) {
        await connectToDb();
        return await Vehicle.findByIdAndUpdate(vehicleId, payload);
    }

    static async delete(vehicleId: string) {
        await connectToDb();
        return await Vehicle.findByIdAndDelete({ _id: vehicleId });
    }

    static async deleteMany(vehicleIds: string[]) {
        await connectToDb();
        return await Vehicle.deleteMany({ _id: { $in: vehicleIds } })
    }
}