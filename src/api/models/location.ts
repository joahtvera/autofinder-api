import { connectToDb } from "../../utils/db";
import { ILocation } from "../interfaces/location";
import Location from "../schemas/location";

export default class LocationModel {

    static async getAll() {
        await connectToDb();
        return await Location.find();
    }

    static async getById(locationId: string) {
        await connectToDb();
        return await Location.findById(locationId);
    }

    static async create(location: ILocation) {
        await connectToDb();
        const newBranch = new Location(location);
        return await newBranch.save();
    }

    static async patch(locationId: string, payload: ILocation) {
        await connectToDb();
        return await Location.findByIdAndUpdate(locationId, payload);
    }

    static async delete(locationId: string) {
        await connectToDb();
        return await Location.findByIdAndDelete({ _id: locationId });
    }
}