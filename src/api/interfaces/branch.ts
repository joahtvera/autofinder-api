import { ILocation } from "./location"

export interface IBranch {
    _id: string
    name: string
    addres: string
    location: ILocation
    createdAt?: Date
    updatedAt?: Date
}