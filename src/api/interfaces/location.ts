export interface ILocation {
    _id?: string
    name: string
    latitude: number
    longitude: number
    createdAt?: Date
    updatedAt?: Date
}