export interface ICategory {
    _id?: string
    name: string
    categoryImage: string
    createdAt?: Date
    updatedAt?: Date
}