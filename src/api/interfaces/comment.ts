export interface IComment {
    _id?: string
    title: string
    body: string
    createdBy: string
    archived: boolean
    createdAt?: Date
    updatedAt?: Date
}