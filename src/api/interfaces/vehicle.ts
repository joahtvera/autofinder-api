export interface IVehicle {
    _id?: string
    brand: string
    category: string
    year: number
    model: string
    imageUrl: string
    branch: string[]
    archived: boolean
    createdAt?: Date
    updatedAt?: Date
}