export interface IUser {
    _id?: string
    username: string
    email: string
    password: string
    favoriteList: string[]
    createdAt?: Date
    updatedAt?: Date
}