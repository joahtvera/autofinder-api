import express from "express";
import UserController from "./controllers/user";
import { connectToDb } from "../utils/db";
import VehicleController from "./controllers/vehicle";
import BranchController from "./controllers/branch";
import LocationController from "./controllers/location";
import CommentController from "./controllers/comment";
import BrandController from "./controllers/brand";
import CategoryController from "./controllers/category";
import UploadController from "./controllers/upload";

const router = express.Router()

router.get("/test", async (request: express.Request, response: express.Response) => {
    await connectToDb()
    return response.status(200).send({
        message: 'success',
        data: [],
        status: 200
    })
});

//Upload
router.post('/upload', UploadController.uploadImage)

//Vehicles
router.get('/vehicles', VehicleController.getAll);
router.get('/vehicles/:id', VehicleController.getById);
router.patch('/vehicles/:id', VehicleController.patch);
router.delete('/vehicles/:id', VehicleController.delete);
router.post('/vehicles', VehicleController.create);

//Users
router.get('/users', UserController.getAll);
router.get('/users/:id', UserController.getById);
router.patch('/users/:id', UserController.patch);
router.delete('/users/:id', UserController.delete);
router.post('/users', UserController.create);

//Branches
router.get('/branches', BranchController.getAll);
router.get('/branches/:id', BranchController.getById);
router.patch('/branches/:id', BranchController.patch);
router.delete('/branches/:id', BranchController.delete);
router.post('/branches', BranchController.create);

//Locations
router.get('/locations', LocationController.getAll);
router.get('/locations/:id', LocationController.getById);
router.patch('/locations/:id', LocationController.patch);
router.delete('/locations/:id', LocationController.delete);
router.post('/locations', LocationController.create);

//Comments
router.get('/comments', CommentController.getAll);
router.get('/comments/:id', CommentController.getById);
router.patch('/comments/:id', CommentController.patch);
router.delete('/comments/:id', CommentController.delete);
router.post('/comments', CommentController.create);

//Brands
router.get('/brands', BrandController.getAll);
router.get('/brands/:id', BrandController.getById);
router.patch('/brands/:id', BrandController.patch);
router.delete('/brands/:id', BrandController.delete);
router.post('/brands', BrandController.create);

//Categories
router.get('/categories', CategoryController.getAll);
router.get('/categories/:id', CategoryController.getById);
router.patch('/categories/:id', CategoryController.patch);
router.delete('/categories/:id', CategoryController.delete);
router.post('/categories', CategoryController.create);

export default router